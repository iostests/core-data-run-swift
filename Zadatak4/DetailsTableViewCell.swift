//
//  DetailsTableViewCell.swift
//  Zadatak4
//
//  Created by Jelena Mehic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import Foundation
import UIKit

class DetailsTableViewCell : UITableViewCell {
    
    @IBOutlet weak var runLengthLabel: UILabel!
    @IBOutlet weak var durationLengthLabel: UILabel!
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}