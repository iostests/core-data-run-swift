//
//  LocationTableViewCell.swift
//  Zadatak4
//
//  Created by Jelena Mehic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import Foundation
import UIKit

//PUBLIC pisemo ili da bude INRERNAL?
class LocationTableViewCell : UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var lonLabel: UILabel!
    @IBOutlet weak var latLabel: UILabel!
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    //init uz pomoc storyboard-a
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
   
}


