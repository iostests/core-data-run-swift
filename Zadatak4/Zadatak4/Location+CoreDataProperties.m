//
//  Location+CoreDataProperties.m
//  Zadatak4
//
//  Created by Jelena Mehic on 10/23/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Location+CoreDataProperties.h"

@implementation Location (CoreDataProperties)

@dynamic lon;
@dynamic lat;
@dynamic myRun;

@end
