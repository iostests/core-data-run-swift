//
//  main.m
//  Zadatak4
//
//  Created by Jelena Mehic on 10/21/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
