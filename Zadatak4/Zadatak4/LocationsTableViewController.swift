//
//  LocationsTableViewController.swift
//  Zadatak4
//
//  Created by Jelena Mehic on 11/3/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import Foundation
import UIKit
//import LocationTableViewCell

class LocationsTableViewController: UITableViewController {
   
    var locations: Set<Location>? {
        didSet {
            locationsArray = locations?.filter({ _ in true });
        }
    }
    private var locationsArray: [Location]?
    
    //DA LI JE STO PONOVO PISEM [Location]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = locationsArray?.count {
            return count;
        } else {
            return 0;
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell1", forIndexPath: indexPath) as! LocationTableViewCell
        
        let loc = locationsArray![indexPath.row]
        cell.lonLabel.text = loc.lon?.stringValue
        cell.latLabel.text = loc.lon?.stringValue      
        
        return cell
    }
    
}

