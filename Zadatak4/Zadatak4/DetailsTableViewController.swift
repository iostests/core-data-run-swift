//
//  DetailsTableViewController.swift
//  Zadatak4
//
//  Created by Jelena Mehic on 11/4/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import UIKit

class DetailsTableViewController: UITableViewController {
    
    private let dateFormatter = NSDateFormatter()
    
    private lazy var fetchedResultsController: NSFetchedResultsController = {
        let fetchRequest = NSFetchRequest(entityName: "Run")
        let primarySort = NSSortDescriptor(key: "date", ascending: true)
        fetchRequest.sortDescriptors = [primarySort]
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: appDelegate.managedObjectContext, sectionNameKeyPath: "dateString", cacheName: nil)
        frc.delegate = self // [[self fetchedResultsController] setDelegate:self];
        //[self setFetchedResultsController:frc]; - NEMA OVDE?
        return frc
    }()
    
    @IBOutlet var tableViewRun: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("An error occurred")
        }
    }
    
    


    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       let sections = fetchedResultsController.sections
        return (sections?.count)!
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let sections = fetchedResultsController.sections
        let currentSection = sections![section]
        return currentSection.numberOfObjects
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! DetailsTableViewCell
        cell.durationLengthLabel.text = ""
        cell.runLengthLabel.text = ""

        guard let run = fetchedResultsController.objectAtIndexPath(indexPath) as? Run else {
            return cell
        }
        
        guard let duration = run.duration, let length = run.length else {
            return cell
        }
        
        cell.durationLengthLabel.text = String(format: "%.2f", duration.doubleValue)
        cell.runLengthLabel.text = String(format: "%.2f", length.doubleValue)
        return cell
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sections = fetchedResultsController.sections
        let currentSection = sections![section]
        return currentSection.name
    }
}
// MARK: - NSFetchedResultsControllerDelegate - 5 funcs
extension DetailsTableViewController: NSFetchedResultsControllerDelegate {
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        switch(type) {
        case NSFetchedResultsChangeType.Insert:
            if let insertIndexPath = newIndexPath {
                tableViewRun.insertRowsAtIndexPaths([insertIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
        case NSFetchedResultsChangeType.Delete:
            if let deleteIndexPath = indexPath {
                tableViewRun.deleteRowsAtIndexPaths([deleteIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
        case NSFetchedResultsChangeType.Update:
            if let updateIndexPath = indexPath {
                // Note that for Update, we update the row at __indexPath__
                let cell = self.tableViewRun.cellForRowAtIndexPath(updateIndexPath) as! DetailsTableViewCell
                let run = self.fetchedResultsController.objectAtIndexPath(updateIndexPath) as? Run
                cell.runLengthLabel.text = run?.length?.stringValue
                cell.durationLengthLabel.text = run?.duration?.stringValue
               
            }
        case NSFetchedResultsChangeType.Move:
            // Note that for Move, we delete the row at __indexPath__
            if let deleteIndexPath = indexPath {
                tableViewRun.deleteRowsAtIndexPaths([deleteIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
            
            // Note that for Move, we insert a row at the __newIndexPath__
            if let insertIndexPath = newIndexPath {
                tableViewRun.insertRowsAtIndexPaths([insertIndexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            }
        }
    }
    
    func controller(
        controller: NSFetchedResultsController,
        didChangeSection sectionInfo: NSFetchedResultsSectionInfo,
        atIndex sectionIndex: Int,
        forChangeType type: NSFetchedResultsChangeType) {
            
            switch type {
            case .Insert:
                let sectionIndexSet = NSIndexSet(index: sectionIndex)
                self.tableViewRun.insertSections(sectionIndexSet, withRowAnimation: UITableViewRowAnimation.Fade)
            case .Delete:
                let sectionIndexSet = NSIndexSet(index: sectionIndex)
                self.tableViewRun.deleteSections(sectionIndexSet, withRowAnimation: UITableViewRowAnimation.Fade)
            default:
                ""
            }
    }
    
    func controller(controller: NSFetchedResultsController, sectionIndexTitleForSectionName sectionName: String) -> String? {
        return sectionName
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableViewRun.endUpdates()
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableViewRun.beginUpdates()
    }
    
}

//MARK: - Navigation
extension DetailsTableViewController {
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.destinationViewController is LocationsTableViewController {
            let locationsController = segue.destinationViewController as! LocationsTableViewController
            let selectedIndexPath = tableViewRun.indexPathForSelectedRow
            let run = fetchedResultsController.objectAtIndexPath(selectedIndexPath!) as! Run
            locationsController.locations = run.locations
            
        }
    }
    
}
