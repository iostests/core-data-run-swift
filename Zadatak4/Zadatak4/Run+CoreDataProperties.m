//
//  Run+CoreDataProperties.m
//  Zadatak4
//
//  Created by Mladjan Antic on 10/31/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Run+CoreDataProperties.h"

@implementation Run (CoreDataProperties)

@dynamic date;
@dynamic duration;
@dynamic length;
@dynamic dateString;
@dynamic locations;

@end
