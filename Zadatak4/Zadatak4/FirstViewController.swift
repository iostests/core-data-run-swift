//
//  FirstViewController.swift
//  Zadatak4
//
//  Created by Jelena Mehic on 11/4/15.
//  Copyright © 2015 iOSAkademija. All rights reserved.
//

import UIKit
import CoreLocation

public class FirstViewController: UIViewController {

    private let manager = CLLocationManager()
    private var timeInterval: NSTimeInterval = 0.0
    private var newRun = false
    private var currentRunObject:Run?
    private var currentLength:float_t = 0.0
    private var lastLocation: CLLocation?
    

    @IBOutlet weak var startOutlet: UIButton!

    @IBOutlet weak var seeProgressOutlet: UIButton!
    
    @IBAction func startActivity(sender: UIButton) {
        
        startOutlet.enabled = false
        seeProgressOutlet.enabled = false
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        manager.startUpdatingLocation()
        timeInterval = NSDate().timeIntervalSince1970
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        currentRunObject = NSEntityDescription.insertNewObjectForEntityForName("Run", inManagedObjectContext: appDelegate.managedObjectContext) as? Run
        currentRunObject!.date = NSDate()
        currentRunObject!.dateString = dateFormatter.stringFromDate(NSDate())
        currentLength = 0.00
        newRun = true
        //save context
        try! appDelegate.managedObjectContext.save()
        
    }
    
    
    @IBAction func stopActivity(sender: UIButton) {
        
        if  startOutlet.enabled == false {
            
            startOutlet.enabled = true
            seeProgressOutlet.enabled = true
            
            manager.stopUpdatingLocation()
            currentRunObject!.length = NSNumber(float: currentLength)
            currentRunObject!.duration = NSDate().timeIntervalSince1970 - timeInterval
            
           let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            try! appDelegate.managedObjectContext.save()
        }
        
        
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.allowsBackgroundLocationUpdates = true
        
        if manager.respondsToSelector(Selector(manager.requestAlwaysAuthorization())) {
            manager.requestAlwaysAuthorization()
        }
        
    }


    /*
    // MARK: - Navigation

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
    
    }
    */

}

// MARK: - CLLocationManagerDelegate

extension FirstViewController : CLLocationManagerDelegate {
    
    public func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last!
        if newRun { newRun = false; lastLocation = location}
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
         //----nova instanca entiteta location
        let locationObject = NSEntityDescription.insertNewObjectForEntityForName("Location", inManagedObjectContext: appDelegate.managedObjectContext) as! Location
        //-----povezivanje instanci
        currentRunObject!.addLocationsObject(locationObject)
        //-----upis vrednosti u instance
        locationObject.lat = NSNumber(double: location.coordinate.latitude)
        locationObject.lon = NSNumber(double: location.coordinate.longitude)
        
        currentLength = currentLength + Float(lastLocation!.distanceFromLocation(location))
        //-----save context
        try! appDelegate.managedObjectContext.save()
    }
    
    
}